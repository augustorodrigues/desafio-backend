# API para Gerenciamento de Horários de Clínica

- José Augusto Santos Rodrigues
- jrodriguesaugusto@gmail.com

## Bibliotecas, linguagem e Frameworks Uutilizados

- Node.js
- Express
- Typescript
- Jest
- Apidoc

## Build, Testes e Documentação

Para instalar as dependências do projeto utilize um dos compandos a baixo:

```bash
npm install
```
ou
```bash
yarn
```

Para iniciar o projeto em modo de desenvolvimento rode os scripts abaixo em dois terminais diferentes com npm ou yarn:

```bash
yarn watch-ts
```

```bash
yarn watch-node
```

ou
```bash
npm run watch-ts
```

```bash
npm run watch-node
```

Caso o arquivo database.json não seja reconhecido durante a execução do projeto, altere o valor da constante "database" da classe DatabaseAccess para o caminho completo do arquivo database.json localizado na pasta desafio-backend/src/database/database.json. Ex: "C:/YourUser/projects/desafio-backend/src/database/database.json".

Para rodar os testes execute o script:

```bash
yarn test
```

Para gerar a documentação atualizada da API é necessário ter o apidoc instalado globalmente e executar o comando:

```bash
yarn global apidoc
```
```bash
apidoc -f routes.ts -o public/apidoc
```
Com o projeto em execução, explore a documentação da api no endereço http://localhost:3000/apidoc.

## Execução

Com o projeto em execução, utilize a Postman Collection (cubos-backend-challenge.postman_collection.json) para utilizar as funcionalidades da API.