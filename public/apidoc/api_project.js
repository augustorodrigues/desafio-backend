define({
  "name": "Schedule Rules API documentation",
  "version": "1.0.0",
  "description": "API schedule rules list manager",
  "template": {
    "forceLanguage": "en"
  },
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2019-09-02T07:38:29.328Z",
    "url": "http://apidocjs.com",
    "version": "0.17.7"
  }
});
